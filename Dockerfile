FROM openjdk:11

ADD ./build/libs/ymoney-test.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]