package ru.kosi.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.kosi.repository.impl.RoundRobinCoordinator;
import ru.kosi.repository.impl.RoundRobinCoordinator.Shard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class RoundRobinCoordinatorTest {

    private static final String FIRST_SHARD_HASH = "111";
    private static final String SECOND_SHARD_HASH = "222";
    private static final String THIRD_SHARD_HASH = "333";

    private static RoundRobinCoordinator roundRobinCoordinator;

    private static Map<String, Shard> shards;

    @BeforeAll
    static void init() {
        roundRobinCoordinator = new RoundRobinCoordinator();
        shards = new HashMap<>();

        Shard firstShard = registerNewShard(FIRST_SHARD_HASH);
        shards.put(firstShard.getHash(), firstShard);

        Shard secondShard = registerNewShard(SECOND_SHARD_HASH);
        shards.put(secondShard.getHash(), secondShard);

        Shard thirdShard = registerNewShard(THIRD_SHARD_HASH);
        shards.put(thirdShard.getHash(), thirdShard);
    }

    @Test
    void testGetAllAliveShards_CoordinatorShouldReturnCorrectNumberOfNodes() {
        Assertions.assertEquals(shards.values().size(), roundRobinCoordinator.getAllAliveShards().size());
    }

    @Test
    void testAliveShardsCount_CoordinatorShouldReturnCorrectNodesCount() {
        Assertions.assertEquals(shards.values().size(), roundRobinCoordinator.aliveShardsCount());
    }

    @Test
    void tesGetAllShards_CoordinatorShouldReturnCorrectNumberOfNodes() {
        Assertions.assertEquals(shards.values().size(), roundRobinCoordinator.getAllShards().size());
    }

    @Test
    void testGetShardByHash_CoordinatorShouldReturn() {
        Assertions.assertEquals(shards.get(FIRST_SHARD_HASH).getJdbcTemplate(),
                roundRobinCoordinator.getShardByHash(FIRST_SHARD_HASH).getJdbcTemplate());
        Assertions.assertEquals(shards.get(SECOND_SHARD_HASH).getJdbcTemplate(),
                roundRobinCoordinator.getShardByHash(SECOND_SHARD_HASH).getJdbcTemplate());
        Assertions.assertEquals(shards.get(THIRD_SHARD_HASH).getJdbcTemplate(),
                roundRobinCoordinator.getShardByHash(THIRD_SHARD_HASH).getJdbcTemplate());
    }

    @Test
    void testGetShardByHash_CoordinatorShouldThrowIllegalStateException() {
        Shard shard = roundRobinCoordinator.getShardByHash(SECOND_SHARD_HASH);
        shard.setAlive(false);

        Assertions.assertThrows(IllegalStateException.class, () -> roundRobinCoordinator.getShardByHash(SECOND_SHARD_HASH));
    }

    @Test
    void testGetNextShard_CoordinatorShouldThrowIllegalStateException() {
        Shard firstShard = roundRobinCoordinator.getShardByHash(FIRST_SHARD_HASH);
        firstShard.setAlive(false);
        Shard secondShard = roundRobinCoordinator.getShardByHash(SECOND_SHARD_HASH);
        secondShard.setAlive(false);
        Shard thirdShard = roundRobinCoordinator.getShardByHash(THIRD_SHARD_HASH);
        thirdShard.setAlive(false);

        Assertions.assertThrows(IllegalStateException.class, () -> roundRobinCoordinator.getNextShard());
    }

    @Test
    void testGetAllAliveShards_CoordinatorShouldThrowIllegalStateException() {
        Shard firstShard = roundRobinCoordinator.getShardByHash(FIRST_SHARD_HASH);
        firstShard.setAlive(false);
        Shard secondShard = roundRobinCoordinator.getShardByHash(SECOND_SHARD_HASH);
        secondShard.setAlive(false);
        Shard thirdShard = roundRobinCoordinator.getShardByHash(THIRD_SHARD_HASH);
        thirdShard.setAlive(false);

        Assertions.assertEquals(0, roundRobinCoordinator.getAllAliveShards().size());
    }

    @Test
    void testRoundRobinAlgorithm_CoordinatorShouldReturnThreeDifferentNodes() {
        List<String> actualShards = new ArrayList<>();
        actualShards.add(roundRobinCoordinator.getNextShard().getHash());
        actualShards.add(roundRobinCoordinator.getNextShard().getHash());
        actualShards.add(roundRobinCoordinator.getNextShard().getHash());

        Assertions.assertTrue(actualShards.contains(FIRST_SHARD_HASH));
        Assertions.assertTrue(actualShards.contains(SECOND_SHARD_HASH));
        Assertions.assertTrue(actualShards.contains(THIRD_SHARD_HASH));
    }

    private static Shard registerNewShard(String shardHash) {
        NamedParameterJdbcTemplate firstJdbcTemplate = new NamedParameterJdbcTemplate(new JdbcTemplate());
        Shard firstShard = Shard.builder()
                .alive(true)
                .hash(shardHash)
                .jdbcTemplate(firstJdbcTemplate)
                .build();
        roundRobinCoordinator.registerShard(firstShard.getHash(), firstShard.getJdbcTemplate());

        return firstShard;
    }

    @AfterEach
    void tearDown() {
        roundRobinCoordinator.getAllShards()
                .forEach(shard -> shard.setAlive(true));
    }
}
