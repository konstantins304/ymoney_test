package ru.kosi.repository.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.kosi.repository.model.Payment.PaymentId;

public class PaymentIdTest {

    @Test
    void testParseId() {
        PaymentId expectedPaymentId = PaymentId.builder()
                .id("1234567")
                .shardId("aaaa")
                .build();

        PaymentId actualPaymentId = PaymentId.parse(expectedPaymentId.toString());
        Assertions.assertEquals(expectedPaymentId.getId(), actualPaymentId.getId());
        Assertions.assertEquals(expectedPaymentId.getShardId(), actualPaymentId.getShardId());
    }
}
