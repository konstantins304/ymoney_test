package ru.kosi.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.testcontainers.containers.PostgreSQLContainer;
import ru.kosi.repository.DbCoordinator;

import javax.sql.DataSource;
import java.time.Duration;

@TestConfiguration
@RequiredArgsConstructor
@Profile({"integrationTest"})
public class TestConfig {

    private final DbCoordinator dbCoordinator;

    @Bean
    public PostgreSQLContainer firstNode() {
        final PostgreSQLContainer postgreSQLContainer = (PostgreSQLContainer) new PostgreSQLContainer("postgres:10")
                .withDatabaseName("postgres")
                .withUsername("postgres")
                .withPassword("postgres")
                .withStartupTimeout(Duration.ofSeconds(60));
        postgreSQLContainer.start();

        return postgreSQLContainer;
    }

    @Bean
    public PostgreSQLContainer secondNode() {
        final PostgreSQLContainer postgreSQLContainer = (PostgreSQLContainer) new PostgreSQLContainer("postgres:10")
                .withDatabaseName("postgres")
                .withUsername("postgres")
                .withPassword("postgres")
                .withStartupTimeout(Duration.ofSeconds(60));
        postgreSQLContainer.start();

        return postgreSQLContainer;
    }

    @Bean
    public PostgreSQLContainer thirdNode() {
        final PostgreSQLContainer postgreSQLContainer = (PostgreSQLContainer) new PostgreSQLContainer("postgres:10")
                .withDatabaseName("postgres")
                .withUsername("postgres")
                .withPassword("postgres")
                .withStartupTimeout(Duration.ofSeconds(60));
        postgreSQLContainer.start();

        return postgreSQLContainer;
    }

    @Bean(name = "firstDataSource")
    public DataSource firstDataSource(final PostgreSQLContainer firstNode, DbProperties properties) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(firstNode.getDriverClassName());
        dataSource.setUrl(firstNode.getJdbcUrl());
        dataSource.setUsername(firstNode.getUsername());
        dataSource.setPassword(firstNode.getPassword());

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setQueryTimeout(5);
        dbCoordinator.registerShard(properties.getNodes().get(0).getName(), new NamedParameterJdbcTemplate(jdbcTemplate));

        return dataSource;
    }

    @Bean(name = "secondDataSource")
    public DataSource secondDataSource(final PostgreSQLContainer secondNode, DbProperties properties) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(secondNode.getDriverClassName());
        dataSource.setUrl(secondNode.getJdbcUrl());
        dataSource.setUsername(secondNode.getUsername());
        dataSource.setPassword(secondNode.getPassword());

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setQueryTimeout(5);
        dbCoordinator.registerShard(properties.getNodes().get(1).getName(), new NamedParameterJdbcTemplate(jdbcTemplate));

        return dataSource;
    }

    @Bean(name = "thirdDataSource")
    public DataSource thirdDataSource(final PostgreSQLContainer thirdNode, DbProperties properties) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(thirdNode.getDriverClassName());
        dataSource.setUrl(thirdNode.getJdbcUrl());
        dataSource.setUsername(thirdNode.getUsername());
        dataSource.setPassword(thirdNode.getPassword());

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setQueryTimeout(5);
        dbCoordinator.registerShard(properties.getNodes().get(2).getName(), new NamedParameterJdbcTemplate(jdbcTemplate));

        return dataSource;
    }
}
