package ru.kosi.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.kosi.controller.dto.PaymentDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;

public class PaymentControllerTest extends AbstractControllerTest {

    private static Stream<Arguments> create3Payments() {
        List<PaymentDTO> payments = new ArrayList<>();
        payments.add(
                PaymentDTO.builder().sender("Отправитель1").consumer("Получатель1").sum("1010.333").build()
        );
        payments.add(
                PaymentDTO.builder().sender("Отправитель2").consumer("Получатель2").sum("1010.333").build()
        );
        payments.add(
                PaymentDTO.builder().sender("Отправитель1").consumer("Получатель3").sum("1010.333").build()
        );

        return Stream.of(Arguments.of(payments));
    }

    private static Stream<Arguments> createEmptyPayment() {
        return Stream.of(Arguments.of(singletonList(PaymentDTO.builder().build())));
    }

    @ParameterizedTest()
    @MethodSource("create3Payments")
    void testLoadPayments_Success(List<PaymentDTO> payments) {
        List<PaymentDTO> responsePayments = executeLoadRequest(payments, new ParameterizedTypeReference<List<PaymentDTO>>() {
        })
                .getBody();

        Assertions.assertNotNull(responsePayments);
        Assertions.assertTrue(responsePayments.stream().anyMatch(payment -> payment.getId().startsWith(dbProperties.getNodes().get(0).getName())));
        Assertions.assertTrue(responsePayments.stream().anyMatch(payment -> payment.getId().startsWith(dbProperties.getNodes().get(1).getName())));
        Assertions.assertTrue(responsePayments.stream().anyMatch(payment -> payment.getId().startsWith(dbProperties.getNodes().get(2).getName())));
    }

    @ParameterizedTest
    @MethodSource("createEmptyPayment")
    void testLoadPayments_withEmptyRequiredPaymentParams_shouldReturnBadRequest(List<PaymentDTO> payments) {
        ResponseEntity<String> responseEntity = executeLoadRequest(payments, new ParameterizedTypeReference<String>() {
        });

        String errorMessage = responseEntity.getBody();
        Assertions.assertNotNull(errorMessage);
        Assertions.assertTrue(errorMessage.contains("Не указан отправитель (sender)"));
        Assertions.assertTrue(errorMessage.contains("Не указан получатель (consumer)"));
        Assertions.assertTrue(errorMessage.contains("Не указана сумма платежа (sum)"));
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @ParameterizedTest
    @MethodSource("create3Payments")
    void testGetTotalSumBySender_Success(List<PaymentDTO> payments) {
        executeLoadRequest(payments, new ParameterizedTypeReference<Object>() {
        });

        ResponseEntity<Map<String, Double>> responseEntity = executeGetTotalSumBySenderRequest("Отправитель1",
                new ParameterizedTypeReference<Map<String, Double>>() {
                });

        Assertions.assertNotNull(responseEntity.getBody());
        Assertions.assertEquals("2020.666", responseEntity.getBody().get("totalSum").toString());
    }

    @Test
    void testGetTotalSum_withEmptySender_ShouldReturnBadRequest() {
        ResponseEntity<String> responseEntity = executeGetTotalSumBySenderRequest("",
                new ParameterizedTypeReference<String>() {
                });

        Assertions.assertNotNull(responseEntity.getBody());
        Assertions.assertEquals("Параметр отправителя (sender) не может быть пустым", responseEntity.getBody());
    }

    @ParameterizedTest
    @MethodSource("create3Payments")
    void testGetPaymentById_Success(List<PaymentDTO> payments) {
        List<PaymentDTO> listLoadedPayments =
                ofNullable(
                        executeLoadRequest(payments, new ParameterizedTypeReference<List<PaymentDTO>>() {
                        }).getBody()
                ).orElse(emptyList());
        PaymentDTO expectedPayment = listLoadedPayments.get(0);

        ResponseEntity<PaymentDTO> responseEntity = executeGetPaymentByIdRequest(expectedPayment.getId(),
                new ParameterizedTypeReference<PaymentDTO>() {
                });
        PaymentDTO actualPayment = responseEntity.getBody();

        Assertions.assertNotNull(actualPayment);
        Assertions.assertEquals(actualPayment.getId(), expectedPayment.getId());
        Assertions.assertEquals(actualPayment.getSender(), expectedPayment.getSender());
        Assertions.assertEquals(actualPayment.getConsumer(), expectedPayment.getConsumer());
        Assertions.assertEquals(actualPayment.getSum(), expectedPayment.getSum());
    }

    @Test
    void testGetPaymentById_ShouldReturnNotFound() {
        String notExistedId = dbProperties.getNodes().get(0).getName() + "_12345";
        ResponseEntity<String> responseEntity = executeGetPaymentByIdRequest(notExistedId,
                new ParameterizedTypeReference<String>() {
                });
        String errorMessage = responseEntity.getBody();

        Assertions.assertNotNull(errorMessage);
        Assertions.assertEquals("Не найден платеж для id = " + notExistedId, errorMessage);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @AfterEach
    void tearDown() {
        dbCoordinator.getAllAliveShards().forEach(shard -> {
            shard.getJdbcTemplate().getJdbcTemplate().execute("TRUNCATE TABLE payments");
        });
    }
}
