package ru.kosi.controller;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kosi.config.DbProperties;
import ru.kosi.config.TestConfig;
import ru.kosi.controller.dto.PaymentDTO;
import ru.kosi.repository.DbCoordinator;

import java.util.List;

import static java.util.Collections.singletonMap;

@SpringBootTest(
        classes = {TestConfig.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@RunWith(SpringRunner.class)
@ActiveProfiles("integrationTest")
@DirtiesContext
public class AbstractControllerTest {

    @Autowired
    protected DbCoordinator dbCoordinator;

    @Autowired
    protected DbProperties dbProperties;

    @Autowired
    private TestRestTemplate restTemplate;

    protected <T> ResponseEntity<T> executeLoadRequest(List<PaymentDTO> payments,
                                                       ParameterizedTypeReference<T> responseType) {
        return restTemplate.exchange(
                "/payment/load",
                HttpMethod.POST,
                new HttpEntity<>(payments),
                responseType
        );
    }

    protected <T> ResponseEntity<T> executeGetTotalSumBySenderRequest(String sender, ParameterizedTypeReference<T> responseType) {
        return restTemplate.exchange(
                "/payment/sum?sender=" + sender,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                responseType
        );
    }

    protected <T> ResponseEntity<T> executeGetPaymentByIdRequest(String id, ParameterizedTypeReference<T> responseType) {
        return restTemplate.exchange(
                "/payment/{id}",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                responseType,
                singletonMap("id", id)
        );
    }
}
