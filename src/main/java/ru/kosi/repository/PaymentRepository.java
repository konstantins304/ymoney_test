package ru.kosi.repository;

import org.springframework.lang.NonNull;
import ru.kosi.controller.dto.PaymentDTO;
import ru.kosi.repository.impl.ShardedPaymentRepository;
import ru.kosi.repository.model.Payment;
import ru.kosi.repository.model.Payment.PaymentId;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Интерфейс, задающий требуемые операции по работе с хранящейся информацией о платеже.
 * Реализован классом {@link ShardedPaymentRepository}
 */
public interface PaymentRepository {

    /**
     * Сохраняет информацию о переданных платежах {@code payments} в системе.
     *
     * @param payments информация о платежах
     * @return список сохраненных платежей
     * @throws IllegalStateException в случае проблем с сохранением
     */
    List<Payment> saveAll(@NonNull List<PaymentDTO> payments);

    /**
     * Возвращает информацию о хранящемся в системе платеже
     * по указанному {@code id}.
     *
     * @param id идентификатор платежа {@link PaymentId}
     * @return информацию о платеже
     * @throws IllegalStateException в случае проблем при поиске
     */
    Optional<Payment> getById(@NonNull PaymentId id);

    /**
     * Возвращает сумму всех платежей по отправителю {@code sender} хранящемся
     * в системе.
     *
     * @param sender отправитель
     * @return сумму или {@literal Optional.empty()}, если в системе нет информации
     * о платежаш для указанного отправителя
     * @throws IllegalStateException в случае проблем при поиске
     */
    Optional<BigDecimal> getSumBySender(@NonNull String sender);
}
