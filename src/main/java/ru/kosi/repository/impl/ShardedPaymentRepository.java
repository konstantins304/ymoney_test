package ru.kosi.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import ru.kosi.controller.dto.PaymentDTO;
import ru.kosi.repository.DbCoordinator;
import ru.kosi.repository.IdGenerator;
import ru.kosi.repository.PaymentRepository;
import ru.kosi.repository.impl.RoundRobinCoordinator.Shard;
import ru.kosi.repository.model.Payment;
import ru.kosi.repository.model.Payment.PaymentId;
import ru.kosi.repository.model.Payment.PaymentRowMapper;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonMap;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

/**
 * Реализация репозитория с поддержкой нескольких нод базы данных
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class ShardedPaymentRepository implements PaymentRepository {

    private static final String GET_SUM_BY_SENDER_QUERY =
            "SELECT SUM(sum)" +
                    " FROM payments" +
                    " WHERE sender = :sender" +
                    " GROUP BY sender";

    private static final String INSERT_QUERY =
            "INSERT INTO payments (id, sender, consumer, sum)" +
                    " values (:id, :sender, :consumer, :sum)";

    private static final String GET_BY_ID_QUERY = "SELECT * FROM payments WHERE id = :id";

    private final DbCoordinator dbCoordinator;
    private final IdGenerator<String> idGenerator;

    /**
     * Сохраняет список платежей на доступных нодах базы данных в
     * соответствии с логикой {@code dbCoordinator}.
     *
     * @param payments список платежей
     * @return список сохраненных платежей
     * @throws IllegalStateException в случае проблем с сохранением
     */
    @Override
    public List<Payment> saveAll(@NonNull List<PaymentDTO> payments) {
        return payments.parallelStream()
                .map(this::save)
                .collect(toList());
    }

    /**
     * Возвращает информацию о хранащемся платеже по указанному {@code id}.
     * Поиск выполняется по ноде базы данных, на которой была сохранена
     * информация о платеже. Нужная нода определяется на основании
     * указанного {@code id}.
     *
     * @param id идентификатор платежа {@link PaymentId}
     * @return информацию о платеже или {@literal Optional.empty()}, если платеж не найден.
     * @throws IllegalStateException в случае проблем при поиске
     */
    @Override
    public Optional<Payment> getById(@NonNull PaymentId id) {
        try {
            return ofNullable(
                    dbCoordinator.getShardByHash(id.getShardId())
                            .getJdbcTemplate()
                            .queryForObject(GET_BY_ID_QUERY, singletonMap("id", id.getId()), new PaymentRowMapper(id.getShardId()))
            );
        } catch (EmptyResultDataAccessException ex) {
            return empty();
        } catch (Exception ex) {
            throw new IllegalStateException(
                    String.format("Ошибка при поиске платежа по id = %s на ноде =  %s", id.getId(), id.getShardId()), ex);
        }
    }

    /**
     * Возвращает сумму всех платежей для указанного отправителя {@code sender}
     * со всех доступных нод базы данных.
     *
     * @param sender отправитель
     * @return сумму всех платежей или {@literal Optional.empty()},
     * если информация о платежах не найдена
     * @throws IllegalStateException в случае проблем при поиске
     */
    @Override
    public Optional<BigDecimal> getSumBySender(@NonNull String sender) {
        List<BigDecimal> sumFromShards = dbCoordinator.getAllAliveShards().stream()
                .map(shard -> this.getSumBySender(shard, sender))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
        if (sumFromShards.isEmpty()) {
            return empty();
        }

        return of(sumFromShards.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    private Payment save(PaymentDTO payment) {
        String id = idGenerator.generate();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        params.addValue("sender", payment.getSender());
        params.addValue("consumer", payment.getConsumer());
        params.addValue("sum", new BigDecimal(payment.getSum()));

        Shard shard;
        do {
            shard = dbCoordinator.getNextShard();
            try {
                shard.getJdbcTemplate()
                        .update(INSERT_QUERY, params);
                break;
            } catch (Exception ex) {
                log.error("Ошибка во время сохранения на ноду = {}", shard.getHash(), ex);
            }
        } while (true);

        return Payment.builder()
                .id(PaymentId.builder().id(id).shardId(shard.getHash()).build())
                .sender(payment.getSender())
                .consumer(payment.getConsumer())
                .sum(new BigDecimal(payment.getSum()))
                .build();
    }

    private Optional<BigDecimal> getSumBySender(Shard shard, String sender) {
        try {
            return ofNullable(shard.getJdbcTemplate()
                    .queryForObject(GET_SUM_BY_SENDER_QUERY, Collections.singletonMap("sender", sender), BigDecimal.class));
        } catch (EmptyResultDataAccessException ex) {
            return empty();
        } catch (Exception ex) {
            throw new IllegalStateException(
                    String.format("Ошибка при поиске информации о платежах по отправителю = %s на ноде = %s",
                            sender, shard.getHash()), ex);
        }
    }
}
