package ru.kosi.repository.impl;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ru.kosi.repository.DbCoordinator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.toList;

/**
 * Реализация координации по нодам базы данных
 * в соответствии с алгоритмом round-robin
 */
@Slf4j
@Component
public class RoundRobinCoordinator implements DbCoordinator {

    private static final AtomicInteger SHARDS_CONTAINER = new AtomicInteger(0);
    private static final List<Shard> SHARDS = new ArrayList<>();

    /**
     * Регистрирует новую ноду базы данных с указанным {@code hash}
     * и {@code jdbcTemplate}.
     *
     * @param hash         ноды базы данных
     * @param jdbcTemplate настроенный {@link NamedParameterJdbcTemplate}
     *                     для доступа к данным
     */
    @Override
    public void registerShard(@NonNull String hash, @NonNull NamedParameterJdbcTemplate jdbcTemplate) {
        SHARDS.add(Shard.builder()
                .hash(hash)
                .jdbcTemplate(jdbcTemplate)
                .alive(true)
                .build());
    }

    /**
     * Возвращает следующую ноду базы данных из списка доступных.
     *
     * @return доступную ноду базы данных
     * @throws IllegalStateException если нет активных нод
     */
    @Override
    public Shard getNextShard() {
        return SHARDS.get(
                SHARDS_CONTAINER
                        .accumulateAndGet(1, (index, increment) -> getNextShardIndex(index))
        );
    }

    /**
     * Возвращает доступную ноду базы данных по указанному {@code hash}.
     *
     * @param hash ноды базы данных
     * @return доступную ноду базы данных
     * @throws IllegalStateException    если нода не активна
     * @throws IllegalArgumentException если передан несуществующий идентификатор ноды
     */
    @Override
    public Shard getShardByHash(@NonNull String hash) {
        Shard shardByHash = SHARDS.stream()
                .filter(shard -> Objects.equals(shard.getHash(), hash))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Неверный идентификатор"));

        if (!shardByHash.isAlive()) {
            throw new IllegalStateException("Не найдено активной ноды по hash  = " + hash);
        }

        return shardByHash;
    }

    /**
     * Возвращает список доступных нод базы данных.
     *
     * @return список нод базы данных
     */
    @Override
    public List<Shard> getAllAliveShards() {
        return SHARDS.stream()
                .filter(Shard::isAlive)
                .collect(toList());
    }

    /**
     * Возаращет кол-во доступных нод базы данных.
     *
     * @return кол-во нод базы данных
     */
    @Override
    public int aliveShardsCount() {
        return (int) SHARDS.stream().filter(Shard::isAlive).count();
    }

    /**
     * Возвращает полный список нод базы данных зарегистрированных
     * в системе.
     *
     * @return список нод
     */
    @Override
    public List<Shard> getAllShards() {
        return SHARDS;
    }

    private int getNextShardIndex(int index) {
        do {
            if (aliveShardsCount() == 0) {
                throw new IllegalStateException("Нет активных шардов");
            }
            index = ++index == SHARDS.size() ? 0 : index;
        } while (!SHARDS.get(index).isAlive());

        return index;
    }

    @Getter
    @Builder
    public static class Shard {
        private final String hash;
        private final NamedParameterJdbcTemplate jdbcTemplate;

        @Setter
        private boolean alive;
    }
}
