package ru.kosi.repository.impl;

import org.springframework.stereotype.Component;
import ru.kosi.repository.IdGenerator;

import java.util.UUID;

/**
 * Реализация {@link IdGenerator} с генерацией id на основе {@link UUID}
 */
@Component
public class UUIDIdGenerator implements IdGenerator<String> {

    /**
     * Возвращает id на полученный при помощи {@literal UUID.randomUUID()}
     *
     * @return id
     */
    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
