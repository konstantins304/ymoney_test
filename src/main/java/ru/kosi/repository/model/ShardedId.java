package ru.kosi.repository.model;

import ru.kosi.repository.model.Payment.PaymentId;

/**
 * Интерфейс описывает шардированый ключ.
 * Реализован классом {@link PaymentId}
 */
public interface ShardedId {

    /**
     * Возвращает идентификатор шарда
     *
     * @return id шарда
     */
    String getShardId();
}
