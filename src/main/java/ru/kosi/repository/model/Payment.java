package ru.kosi.repository.model;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

@Getter
@Builder
public class Payment {

    private final PaymentId id;
    private final String sender;
    private final String consumer;
    private final BigDecimal sum;

    @Getter
    @Builder
    public static class PaymentId implements ShardedId {
        private final String id;
        private final String shardId;

        public static PaymentId parse(String paymentId) {
            String[] idParams = paymentId.split("_");

            return PaymentId.builder().id(idParams[1]).shardId(idParams[0]).build();
        }

        @Override
        public String toString() {
            return shardId + "_" + id;
        }
    }

    @RequiredArgsConstructor
    public static class PaymentRowMapper implements RowMapper<Payment> {

        private final String shardId;

        @Override
        public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
            PaymentId id = PaymentId.builder()
                    .id(rs.getString("id"))
                    .shardId(shardId)
                    .build();

            return Payment.builder()
                    .id(id)
                    .sender(rs.getString("sender"))
                    .consumer(rs.getString("consumer"))
                    .sum(rs.getBigDecimal("sum"))
                    .build();
        }
    }
}
