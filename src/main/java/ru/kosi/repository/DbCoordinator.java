package ru.kosi.repository;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.lang.NonNull;
import ru.kosi.repository.impl.RoundRobinCoordinator;
import ru.kosi.repository.impl.RoundRobinCoordinator.Shard;

import java.util.List;

/**
 * Интерфейс, задающий базовый набор операций для работы с множеством нод
 * базы данных. Реализован классом {@link RoundRobinCoordinator}.
 */
public interface DbCoordinator {

    /**
     * Позволяет зарегистрировать в системе новую ноду базы данных
     * с указанным {@code hash} и {@code jdbcTemplate}.
     *
     * @param hash         ноды
     * @param jdbcTemplate настроенный {@link NamedParameterJdbcTemplate}
     *                     для доступа к данным
     */
    void registerShard(@NonNull String hash, @NonNull NamedParameterJdbcTemplate jdbcTemplate);

    /**
     * Возвращает ноду базы данных (зависит от реализации см. {@link RoundRobinCoordinator}).
     *
     * @return ноду базы данных
     * @throws IllegalStateException если в системе нет активных нод
     */
    Shard getNextShard();

    /**
     * Возвращает ноду базы данных по указанному {@code hash}
     * (зависит от реализации см. {@link RoundRobinCoordinator}).
     *
     * @param hash ноды
     * @return ноду базы данных
     * @throws IllegalStateException    в случае проблем с нодой
     * @throws IllegalArgumentException если передан неверный идентификатор
     */
    Shard getShardByHash(@NonNull String hash);

    /**
     * Возвращает список доступных в системе нод базы данных.
     *
     * @return список нод
     */
    List<Shard> getAllAliveShards();

    /**
     * Возвращает кол-во доступных в системе нод базы данных.
     *
     * @return кол-во нод
     */
    int aliveShardsCount();

    /**
     * Возвращает полный список нод базы данных зарегистрированных
     * в системе.
     *
     * @return список нод
     */
    List<Shard> getAllShards();
}
