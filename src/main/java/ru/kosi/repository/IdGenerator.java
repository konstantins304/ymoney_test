package ru.kosi.repository;

import ru.kosi.repository.impl.UUIDIdGenerator;

/**
 * Интерфейс для генераторов индентификаторов.
 * Реализован классом {@link UUIDIdGenerator}.
 */
public interface IdGenerator<T> {

    /**
     * Возвращает идентификатор.
     *
     * @return идентификатор
     */
    T generate();
}
