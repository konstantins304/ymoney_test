package ru.kosi.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class LiquibaseConfig {

    private static SpringLiquibase springLiquibase(DataSource dataSource, LiquibaseProperties properties) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog(properties.getChangeLog());
        liquibase.setContexts(properties.getContexts());
        liquibase.setDefaultSchema(properties.getDefaultSchema());
        liquibase.setShouldRun(properties.isEnabled());
        liquibase.setLabels(properties.getLabels());
        liquibase.setChangeLogParameters(properties.getParameters());
        liquibase.setRollbackFile(properties.getRollbackFile());

        return liquibase;
    }

    @Bean
    public SpringLiquibase liquibase(DataSource firstDataSource,
                                     LiquibaseProperties liquibaseProperties) {
        return springLiquibase(firstDataSource, liquibaseProperties);
    }

    @Bean
    public SpringLiquibase twoLiquibase(DataSource secondDataSource,
                                        LiquibaseProperties liquibaseProperties) {
        return springLiquibase(secondDataSource, liquibaseProperties);
    }

    @Bean
    public SpringLiquibase thirdLiquibase(DataSource thirdDataSource,
                                          LiquibaseProperties liquibaseProperties) {
        return springLiquibase(thirdDataSource, liquibaseProperties);
    }
}
