package ru.kosi.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.kosi.repository.DbCoordinator;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
@Profile({"production"})
public class DataSourceConfig {

    private final DbCoordinator dbCoordinator;

    private static DataSource dataSource(String url) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(url);
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");

        return dataSource;
    }

    @Bean
    public DataSource firstDataSource(DbProperties dbProperties) {
        DataSource dataSource = dataSource(dbProperties.getNodes().get(0).getUrl());

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setQueryTimeout(dbProperties.getQueryTimeout());
        dbCoordinator.registerShard(dbProperties.getNodes().get(0).getName(), new NamedParameterJdbcTemplate(jdbcTemplate));

        return dataSource;
    }

    @Bean
    public DataSource secondDataSource(DbProperties dbProperties) {
        DataSource dataSource = dataSource(dbProperties.getNodes().get(1).getUrl());

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setQueryTimeout(dbProperties.getQueryTimeout());
        dbCoordinator.registerShard(dbProperties.getNodes().get(1).getName(), new NamedParameterJdbcTemplate(jdbcTemplate));

        return dataSource;
    }

    @Bean
    public DataSource thirdDataSource(DbProperties dbProperties) {
        DataSource dataSource = dataSource(dbProperties.getNodes().get(2).getUrl());

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setQueryTimeout(dbProperties.getQueryTimeout());
        dbCoordinator.registerShard(dbProperties.getNodes().get(2).getName(), new NamedParameterJdbcTemplate(jdbcTemplate));

        return dataSource;
    }
}
