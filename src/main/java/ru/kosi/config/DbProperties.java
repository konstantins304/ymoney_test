package ru.kosi.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "db")
public class DbProperties {

    /**
     * Список нод базы данных
     */
    @Size(min = 3, max = 3)
    private List<Node> nodes = new ArrayList<>();

    /**
     * Таймаут на время выполнения запроса к базе данных
     */
    @Positive
    private int queryTimeout;

    /**
     * Задержка между попытками healthCheck'а базы данных
     */
    @Positive
    private int healthCheckDelay;

    @Setter
    @Getter
    public static class Node {

        /**
         * Название - идентификатор базы данных в системе
         */
        @NotBlank(message = "Необходимо указать название ноды базы данных")
        private String name;

        /**
         * Jdbc url
         */
        @NotBlank(message = "Необходимо указать jdbc url для доступа к базе данных")
        private String url;
    }
}
