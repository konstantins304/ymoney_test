package ru.kosi.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ru.kosi.repository.DbCoordinator;

@Slf4j
@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class DbHealthChecker {

    private final DbCoordinator dbCoordinator;

    @Scheduled(fixedDelayString = "${db.health-check-delay}")
    public void healthCheck() {
        dbCoordinator.getAllShards().forEach(shard -> {
            try {
                shard.getJdbcTemplate().getJdbcTemplate().execute("SELECT 1");
                shard.setAlive(true);
            } catch (Exception ex) {
                if (shard.isAlive()) {
                    log.error("Нода = {} недоступна!", shard.getHash(), ex);
                }
                shard.setAlive(false);
            }
        });
    }
}
