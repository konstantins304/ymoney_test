package ru.kosi.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.kosi.controller.dto.PaymentDTO;
import ru.kosi.controller.dto.SumBySenderDTO;
import ru.kosi.service.PaymentService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.kosi.controller.PaymentController.ROOT_PATH;

@Validated
@RestController
@RequestMapping(ROOT_PATH)
@RequiredArgsConstructor
public class PaymentController {

    static final String ROOT_PATH = "/payment";
    private static final String BY_ID_PAYMENT_PATH = "/{id}";
    private static final String LOAD_PATH = "/load";
    private static final String SUM = "/sum";

    private final PaymentService paymentService;

    @ApiOperation(value = "Загрузка платежей в систему", response = PaymentDTO.class)
    @ApiResponses({
            @ApiResponse(code = 400, message = "Не указан отправитель (sender)\nНе указан получатель (consumer)\nНе указана сумма платежа (sum)"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера, пожалуйста, обратитесь к системному администратору")
    })
    @PostMapping(path = LOAD_PATH, consumes = APPLICATION_JSON_VALUE)
    public List<PaymentDTO> load(
            @ApiParam(name = "payments", value = "Список платежей", required = true)
            @RequestBody List<@Valid PaymentDTO> payments) {
        return paymentService.load(payments);
    }

    @ApiOperation(value = "Получение информации о платеже по идентификатору", response = PaymentDTO.class)
    @ApiResponses({
            @ApiResponse(code = 404, message = "Не найден платеж для id = {идентификатор}"),
            @ApiResponse(code = 400, message = "Указан неверный идентификатор"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера, пожалуйста, обратитесь к системному администратору")
    })
    @GetMapping(path = BY_ID_PAYMENT_PATH, produces = APPLICATION_JSON_VALUE)
    public PaymentDTO getById(
            @ApiParam(name = "id", value = "Идентификатор платежа", required = true)
            @PathVariable String id) {
        return paymentService.getById(id);
    }

    @ApiOperation(value = "Получение общей суммы всех платежей по отправителю", response = SumBySenderDTO.class)
    @ApiResponses({
            @ApiResponse(code = 404, message = "Не найдено ни одного платежа для отправителя = {имя отправителя}"),
            @ApiResponse(code = 400, message = "Параметр отправителя (sender) не может быть пустым"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервера, пожалуйста, обратитесь к системному администратору")
    })
    @GetMapping(path = SUM, produces = APPLICATION_JSON_VALUE)
    public SumBySenderDTO getSumBySender(
            @ApiParam(name = "sender", value = "Имя отправителя", required = true)
            @RequestParam @NotEmpty(message = "Параметр отправителя (sender) не может быть пустым") String sender) {
        return new SumBySenderDTO(paymentService.getSumBySender(sender).toString());
    }
}
