package ru.kosi.controller.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SumBySenderDTO {

    private final String totalSum;
}
