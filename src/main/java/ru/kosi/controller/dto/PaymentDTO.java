package ru.kosi.controller.dto;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Builder
public class PaymentDTO {

    private final String id;
    @NotBlank(message = "Не указан отправитель (sender)")
    private final String sender;
    @NotBlank(message = "Не указан получатель (consumer)")
    private final String consumer;
    @NotNull(message = "Не указана сумма платежа (sum)")
    private final String sum;
}
