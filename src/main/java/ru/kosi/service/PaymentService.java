package ru.kosi.service;

import org.springframework.lang.NonNull;
import ru.kosi.controller.dto.PaymentDTO;
import ru.kosi.exception.NotFoundException;
import ru.kosi.service.impl.DefaultPaymentService;

import java.math.BigDecimal;
import java.util.List;

/**
 * Интерфейс, задающий требуемый набор операций над
 * информацией о платежах. Реализован классом {@link DefaultPaymentService}.
 */
public interface PaymentService {

    /**
     * Позволяет загрузить информацию о платежах {@code payments} в систему.
     *
     * @param payments информация о платежах
     * @return список сохраненных в системе платежей
     * @throws IllegalStateException в случае проблем с сохранением
     */
    List<PaymentDTO> load(@NonNull List<PaymentDTO> payments);

    /**
     * Позволяет получить информацию хранящуюся в системе о платеже
     * по идентификатору {@code id}
     *
     * @param id идентификатор платежа
     * @return инормацию о платеже
     * @throws NotFoundException     если информация о платеже не найдена
     * @throws IllegalStateException в случае проблем при поиске
     */
    PaymentDTO getById(@NonNull String id);

    /**
     * Позволяет получить сумму всех платежей, хранящихся в системе,
     * по отправителю {@code sender}.
     *
     * @param sender информацюи об отправителе
     * @return сумма всех платежей
     * @throws NotFoundException     если в системе нет данных о платежах
     *                               для данного отправителя
     * @throws IllegalStateException в случае проблем при поиске
     */
    BigDecimal getSumBySender(@NonNull String sender);
}
