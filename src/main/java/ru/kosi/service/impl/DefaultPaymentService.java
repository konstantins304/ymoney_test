package ru.kosi.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import ru.kosi.controller.dto.PaymentDTO;
import ru.kosi.exception.NotFoundException;
import ru.kosi.repository.PaymentRepository;
import ru.kosi.repository.model.Payment;
import ru.kosi.repository.model.Payment.PaymentId;
import ru.kosi.service.PaymentService;

import java.math.BigDecimal;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Базовая реализация сервиса по работе с платежами.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DefaultPaymentService implements PaymentService {

    private final PaymentRepository paymentRepository;

    /**
     * Позволяет загрузить информацию о платежах {@code payments}
     * в систему.
     *
     * @param payments информация о платежах
     * @return список сохраненных в системе платежей
     * @throws IllegalStateException в случае проблем с сохранением
     */
    @Override
    public List<PaymentDTO> load(@NonNull List<PaymentDTO> payments) {
        return paymentRepository.saveAll(payments).stream()
                .map(payment -> PaymentDTO.builder()
                        .id(payment.getId().toString())
                        .sender(payment.getSender())
                        .consumer(payment.getConsumer())
                        .sum(payment.getSum().toString())
                        .build())
                .collect(toList());
    }

    /**
     * Позволяет получитб информацию хранящуюся в системе о платеже
     * по идентификатору {@code id}
     *
     * @param id идентификатор платежа
     * @return инормацию о платеже
     * @throws NotFoundException     если информация о платеже не найдена
     * @throws IllegalStateException в случае проблем при поиске
     */
    @Override
    public PaymentDTO getById(@NonNull String id) {
        Payment payment = paymentRepository.getById(PaymentId.parse(id))
                .orElseThrow(() -> new NotFoundException("Не найден платеж для id = " + id));

        return PaymentDTO.builder()
                .id(payment.getId().toString())
                .sender(payment.getSender())
                .consumer(payment.getConsumer())
                .sum(payment.getSum().toString())
                .build();
    }

    /**
     * Позволяет получить сумму всех платежей, хранящихся в системе,
     * по отправителю {@code sender}.
     *
     * @param sender информацюи об отправителе
     * @return сумма всех платежей
     * @throws NotFoundException     если в системе нет данных о платежах
     *                               для данного отправителя
     * @throws IllegalStateException в случае проблем при поиске
     */
    @Override
    public BigDecimal getSumBySender(@NonNull String sender) {
        return paymentRepository.getSumBySender(sender)
                .orElseThrow(() -> new NotFoundException("Не найдено ни одного платежа для отправителя = " + sender));
    }
}
