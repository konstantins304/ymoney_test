package ru.kosi.exception;

/**
 * Исключение может быть брошено, если не найдено
 * ни одной записи в базе данных по указанным параметрам
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
}
